## PyCharm RPM packaging using FPM


I'm not much of an rpm-spec writer, so it's
more convenient for me to use fpm.

It's not supposed to be used in any production
environments.

```bash
$ ./build.sh 2018.1.4
```

```bash
$ sudo ./publish.sh
```

```bash
$ sudo ./install.sh
```
