#!/bin/bash
set -x
set -e

# By the convension like 2018.1.4
readonly version="${1}"
readonly repo="${2}"
readonly tmproot="$(pwd)/buildroot"
readonly sourceroot="$(pwd)/sourceroot"
readonly app_name="pycharm-professional-${version}"
readonly tarball_name="${app_name}.tar.gz"
readonly tarball_source_location="${sourceroot}/${tarball_name}"
readonly app_result_dir="/opt/pycharm-${version}"
readonly app_build_dir="${tmproot}${app_result_dir}"
readonly desktop_file="${tmproot}/usr/share/applications/pycharm-professional.desktop"
readonly package_name="pycharm-professional"

if [[ -z "${version}" ]]; then
    echo "Version is not specified"
    exit 1
fi

rm --verbose \
   --recursive \
   --force \
   "${tmproot}"

mkdir --parents \
    "${tmproot}/usr/share/applications" \
    "${tmproot}/usr/share/icons/hicolor/128x128/apps" \
    "${tmproot}/usr/share/icons/gnome/128x128/apps" \
    "${tmproot}/opt/" \
    "${sourceroot}"

if [[ ! -f "${tarball_source_location}" ]]; then
    wget "https://download.jetbrains.com/python/${tarball_name}" \
        --output-document="${tarball_source_location}"
fi

# Extract the given source tarball to /opt
tar --verbose \
    --extract \
    --gzip \
    --file "${tarball_source_location}" \
    --directory "${tmproot}/opt"

cat << EOF > "${desktop_file}"
[Desktop Entry]
Version=1.0
Type=Application
Name=PyCharm Professional Edition
Icon=${app_result_dir}/bin/pycharm.png
Exec="${app_result_dir}/bin/pycharm.sh" %f
Comment=The Drive to Develop
Categories=Development;IDE;
Terminal=false
StartupWMClass=jetbrains-pycharm
EOF

chmod --verbose 644 "${desktop_file}"

cp --verbose \
   --force \
   "${app_build_dir}/bin/pycharm.png" \
   "${tmproot}/usr/share/icons/hicolor/128x128/apps/"

cp --verbose \
   --force \
   "${app_build_dir}/bin/pycharm.png" \
   "${tmproot}/usr/share/icons/gnome/128x128/apps/"

# It causes an error
rm --verbose \
   --recursive \
   --force \
   "${app_build_dir}/plugins/textmate"

fpm --verbose \
    --debug \
    --input-type dir \
    --output-type rpm \
    --name "${package_name}" \
    --version "${version}" \
    --architecture noarch \
    --chdir "${tmproot}"

rm --verbose \
   --recursive \
   --force \
   "${tmproot}" \
   "${sourceroot}"
