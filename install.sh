#!/usr/bin/env bash

dnf clean all

action="update"
rpm -q pycharm-professional &> /dev/null
code=$?
if [ $code -ne 0 ]; then
  action="install"
fi

dnf "${action}" \
    --assumeyes \
    --disablerepo "*" \
    --enablerepo "denis-local-pycharm" \
    pycharm-professional
