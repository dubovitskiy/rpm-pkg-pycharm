#!/usr/bin/env bash
readonly repository="/var/repo/denis-local/pycharm"
readonly repo_file="$(pwd)/denis-local-pycharm.repo"

mkdir --parents "${repository}"

cp --verbose \
   --force "${repo_file}" \
   /etc/yum.repos.d/denis-local-pycharm.repo

mv --verbose \
   --force $(pwd)/*.rpm \
   "${repository}"

createrepo "${repository}"

